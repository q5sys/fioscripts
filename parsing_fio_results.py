import os
import subprocess
from pathlib import Path

def process_file(file_path):
    pattern_query = '.jobs[0]."job options".rw'
    jobs_query = '.jobs[0]."job options".numjobs'
    iodepth_query = '.jobs[0]."job options".iodepth'
    bs_query = '.jobs[0]."job options".bs'
    write_bandwidth_query = '.jobs[0].write.bw'
    write_iops_query = '.jobs[0].write.iops'
    read_bandwidth_query = '.jobs[0].read.bw'
    read_iops_query = '.jobs[0].read.iops'

    pattern = subprocess.run(
        ['jq', pattern_query, file_path],
        capture_output=True,
        text=True
    ).stdout.strip()

    jobs = subprocess.run(
        ['jq', jobs_query, file_path],
        capture_output=True,
        text=True
    ).stdout.strip()

    iodepth = subprocess.run(
        ['jq', iodepth_query, file_path],
        capture_output=True,
        text=True
    ).stdout.strip()

    bs = subprocess.run(
        ['jq', bs_query, file_path],
        capture_output=True,
        text=True
    ).stdout.strip()

    write_bandwidth = subprocess.run(
        ['jq', write_bandwidth_query, file_path],
        capture_output=True,
        text=True
    ).stdout.strip()

    write_iops = subprocess.run(
        ['jq', write_iops_query, file_path],
        capture_output=True,
        text=True
    ).stdout.strip()

    read_bandwidth = subprocess.run(
        ['jq', read_bandwidth_query, file_path],
        capture_output=True,
        text=True
    ).stdout.strip()

    read_iops = subprocess.run(
        ['jq', read_iops_query, file_path],
        capture_output=True,
        text=True
    ).stdout.strip()


    return pattern, jobs, iodepth, bs, write_bandwidth, write_iops, read_bandwidth, read_iops

def main():
    json_directory = './'
    output_file_path = 'results.csv'
    with open(output_file_path, 'w') as output_file:
        for file in os.listdir(json_directory):
            if file.endswith('.json'):
                file_path = os.path.join(json_directory, file)
                pattern, jobs, iodepth, bs, write_bandwidth, write_iops, read_bandwidth, read_iops = process_file(file_path)
                output_file.write(f"{file},{pattern},{jobs},{iodepth},{bs},{write_bandwidth},{write_iops},{read_bandwidth},{read_iops} ")
                output_file.write("\n")


if __name__ == "__main__":
    main()
