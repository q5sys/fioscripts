#!/bin/sh
# Set Variables
NAME="testname"
RECORDSIZES="4k 16k 32k 128k 256k 1m"
LOOPS="01 02 03"
JOBS="1 8 16 32"
IODEPTH=16
SIZE="10g"
RUNTIME=120
POOLNAME="poolname"


# Create datasets for testing
zfs create ${POOLNAME}/bench || :
for b in $RECORDSIZES; do
        zfs create -o recordsize=$b -o primarycache=metadata ${POOLNAME}/bench/$b || :
done

# Run the testing
# Note: Make sure to verify the mountpoint of the pool and change the --directory= path accordingly
echo "TESTING"
mkdir ./${NAME}
for b in $RECORDSIZES; do
        for j in $JOBS; do
                # Create files to benchmark reads with
                fio --directory=/${POOLNAME}/bench/$b --name=bench_$b_$j --rw=write --bs=$b --numjobs=$j --iodepth=${IODEPTH} --size=${SIZE} --end_fsync=1 --ioengine=posixaio --group_reporting --fallocate=none --io_size=${SIZE} --output-format=json --output=./${NAME}/${POOLNAME}_${b}_${j}_${l}_seqwrite.json
                time zpool sync ${POOLNAME}
                time zpool sync ${POOLNAME}
                # Run sequential read tests
                for l in $LOOPS; do
                        # Clear cache
                        zpool export ${POOLNAME}
                        zpool import ${POOLNAME}
                        fio --directory=/${POOLNAME}/bench/$b --name=bench_$b_$j --rw=read --bs=$b --numjobs=$j --iodepth=${IODEPTH} --size=${SIZE} --end_fsync=1 --ioengine=posixaio --group_reporting --fallocate=none --runtime=$RUNTIME --time_based --readonly --output-format=json --output=./${NAME}/${POOLNAME}_${b}_${j}_${l}_seqread.json
                done
                # Run random read tests
                for l in $LOOPS; do
                        # Clear cache
                        zpool export ${POOLNAME}
                        zpool import ${POOLNAME}
                        fio --directory=/${POOLNAME}/bench/$b --name=bench_$b_$j --rw=randread --bs=$b --numjobs=$j --iodepth=${IODEPTH} --size=${SIZE} --end_fsync=1 --ioengine=posixaio --group_reporting --fallocate=none --runtime=$RUNTIME --time_based --readonly --output-format=json --output=./${NAME}/${POOLNAME}_${b}_${j}_${l}_randread.json
                done
                # Run async write tests
                for l in $LOOPS; do
                        fio --directory=/${POOLNAME}/bench/$b --name=bench_$b_$j --rw=randwrite --bs=$b --numjobs=$j --iodepth=${IODEPTH} --size=${SIZE} --end_fsync=1 --ioengine=posixaio --group_reporting --fallocate=none --runtime=$RUNTIME --time_based --output-format=json --output=./${NAME}/${POOLNAME}_${b}_${j}_${l}_randwrite_async.json
                        time zpool sync ${POOLNAME}
                done
                # Run sync write tests
                for l in $LOOPS; do
                        fio --directory=/${POOLNAME}/bench/$b --name=bench_$b_$j --rw=randwrite --bs=$b --numjobs=$j --iodepth=${IODEPTH} --size=${SIZE} --end_fsync=1 --fsync=1 --ioengine=posixaio --group_reporting --fallocate=none --runtime=$RUNTIME --time_based --output-format=json --output=./${NAME}/${POOLNAME}_${b}_${j}_${l}_randwrite_sync.json
                        time zpool sync ${POOLNAME}
                done
        done
done
time zpool sync ${POOLNAME}

# Save Results and Clean Up

cp ${NAME}.sh ./${NAME}/${NAME}.sh
tar zcf ${NAME}.tar.gz ./${NAME}/*
#zfs destroy -fr ${POOLNAME}/bench
